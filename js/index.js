//Variables
moment().locale('es');

let taskList = document.querySelector('#taskList')
    taskTitleInput = document.querySelector('#taskTitle')
    taskDescriptionInput = document.querySelector('#taskDescription')
    btnTaskSubmit = document.querySelector('#taskSubmit')

let inputDate
let dateEntered

//Funciones
document.querySelector('#dateTask').addEventListener('change', function() {
    inputDate =  this.value
    dateEntered = moment(inputDate).format('DD/MM/YYYY')
    console.log(dateEntered)
    return dateEntered
})

const addTask = (e) => {

    let title = taskTitleInput.value
        description = taskDescriptionInput.value
        date = dateEntered
        //Creamos un div para envolver las tareas
        taskWrap = document.createElement('div')
        taskWrap.className = 'taskWrap'
        //Creamos los elementos para las tareas
        newTitle = document.createElement('li')
        newDescription = document.createElement('p')
        newDate = document.createElement('p')

        btnDeleteTask = document.createElement('button')
        btnDeleteTask.type = 'button'
        btnDeleteTask.className = 'btnDeleteTask'
        btnDeleteTask.innerText = 'Eliminar'

        contentTitle = document.createTextNode(title)
        contentDescription = document.createTextNode(description)
        contentDate = document.createTextNode(date)

        if(title !== '' || description !== '' || date !==''){
           //Agrgamos los textos de li y p a las titulos y las descripciones
            newTitle.appendChild(contentTitle)
            newDescription.appendChild(contentDescription)
            newDate.appendChild(contentDate)
            //Ponemos los elemnetos dentro del div de "taskWrap"
            taskWrap.appendChild(newTitle)
            taskWrap.appendChild(newDescription)
            taskWrap.appendChild(btnDeleteTask)
            taskWrap.appendChild(newDate)
            //Ponemos el div dentro de la lista
            taskList.appendChild(taskWrap)
        } else {
            alert('Debes agregar todos los campos')
            return false
        }

        console.log(title)
        console.log(description)
        console.log(date)

        document.querySelector('.taskForm').reset()

        btnDeleteTask.addEventListener('click', function(){
            this.parentNode.remove(this)
        })

        e.preventDefault()
}


btnTaskSubmit.addEventListener('click', addTask)